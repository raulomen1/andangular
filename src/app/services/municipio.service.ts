import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class MunicipioService {
    url = 'http://100.24.232.62:8085/api/APruebaTecnica/ObtenerMunicipios'
    constructor(
        private http: HttpClient
    ) {
        console.log('Servicio Municipio')
    }
    getMunicipio(codDep: number) {
        let header = new HttpHeaders()
            .set('Type-content', 'aplication/json')
        return this.http.get(this.url+"/"+codDep, { headers: header });
    }
}
