import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class TramiteService {
    url = 'http://100.24.232.62:8085/api/APruebaTecnica/ObtenerData'
    constructor(
        private http: HttpClient
    ) {
        console.log('Servicio Tramite')
    }
    getTramite() {
        let header = new HttpHeaders()
            .set('Type-content', 'aplication/json')
        return this.http.get(this.url, { headers: header });
    }
}
